
#ifndef AVGCOLORINDIRECTORY_H
#define AVGCOLORINDIRECTORY_H


#include "AverageColor.h"
#include <QDir>
#include <QString>
#include <QDebug>
#include <QElapsedTimer>
#include <QDirIterator>

class AvgColorInDirectory : public AverageColor
{

public:
    AvgColorInDirectory(QDir Directory, int subPictureWidth, int subPictureHeight, QString Format, int noOfPictures = 1);
    void CalculateALL() override;

    int GetArraySize() const override;

    virtual ~AvgColorInDirectory();


private:

    void ChangeResults();
    //void CheckResultAndName(const QString &Path, const int &Count);

private:
    QDir m_Directory;
    QString m_Format;
    std::shared_ptr<QImage> m_Picture=std::make_shared<QImage>();
    int m_ArraySize;

};

#endif // !AVGCOLORINDIRECTORY_H
