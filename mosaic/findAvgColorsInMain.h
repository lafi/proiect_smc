#ifndef FINDAVGCOLORSINMAIN_H
#define FINDAVGCOLORSINMAIN_H

#include "AverageColor.h"
#include <QImage>
#include <QString>
#include <QElapsedTimer>
#include <QFile>
#include <QDebug>
#include <QTextStream>
#include <QRect>

class FindAvgColorsInMain:public AverageColor
{
public:

    FindAvgColorsInMain(QString PictureName, int subPictureWidth, int subPictureHeight, int numOfSubPictures, int Precision = 1);
    void CalculateALL() override;
    int GetArraySize() const override;
    virtual ~FindAvgColorsInMain();

private:

    int m_numOfSubPics;
    QString m_PictureName;
    std::shared_ptr<QImage> m_Picture=std::make_shared<QImage>();


};
#endif
