#ifndef PHOTOMOSAIC_H
#define PHOTOMOSAIC_H

#include<cmath>
#include <QImage>
#include <QPainter>
#include <QObject>
#include <QDir>
#include <QtConcurrent/QtConcurrentRun>
#include <QFuture>
#include <QFutureWatcher>
#include <memory>
#include "AverageColor.h"
#include "AvgColorInDirectory.h"
#include "findAvgColorsInMain.h"

class PhotoMosaic : public QObject
{
    Q_OBJECT;

public:

    PhotoMosaic();

public:

    void SetSubImageWidth(int Width);
    void SetSubImageHeight(int Height);
    void RunConcurrent(QVector <QString> Vec);

private:

    int subImageWidth;
    int subImageHeight;

    QString ResizeImg(QString img_path, int x, int y)const;
    QStringList* ColorDifference(std::vector<AverageColor::RGB> DirArray, int DirArraySize, std::vector<AverageColor::RGB> MainArray,int MainArraySize, int Precision=1) const;

private:

    void Run(QVector<QString> Vector);
    void ConstructImage(QStringList* sub_Img, std::shared_ptr<QImage> Destination, QString sub_ImgDir) const;
    double EuclideanDistanceRGB(AvgColorInDirectory::RGB R1, AvgColorInDirectory::RGB R2) const;


};
#endif // PHOTOMOSAIC_H
