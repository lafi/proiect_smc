#include "findAvgColorsInMain.h"
#include <memory>
FindAvgColorsInMain::FindAvgColorsInMain(QString picName, int subPicWidth, int subPicHeight, int numOfSubPictures, int precision)
    :AverageColor(subPicWidth, subPicHeight, precision)
{
    m_numOfSubPics = numOfSubPictures;
    m_PictureName = picName;
    m_AverageRGBArray.reserve(m_numOfSubPics * m_noOfDividingPictures * m_noOfDividingPictures);
}

void FindAvgColorsInMain::CalculateALL()
{
    m_Picture->load(m_PictureName);

    int currentX = 0;
    int currentY = 0;
    int picsOnX = m_Picture->width() / m_subPictureWidth;
    int picsOnY = m_Picture->height() / m_subPictureHeight;

    int count = 0;
    for (int i = 0; i < picsOnY; i++) {
        for (int j = 0; j < picsOnX; j++)
        {
            QImage subImage = CreateSubImages(m_Picture, currentX, currentY, m_subPictureWidth, m_subPictureHeight);

            currentX += m_subPictureWidth;

            if (m_noOfDividingPictures > 1)
            {

                std::shared_ptr<QImage> m_subImage;
                std::vector<RGB> arr = AverageColor::HigherPrecision(m_subImage);
                for (int i = 0; i < m_noOfDividingPictures * m_noOfDividingPictures; i++)
                {
                    m_AverageRGBArray[count + i] = arr[i];
                }
                arr.clear();
                count += m_noOfDividingPictures * m_noOfDividingPictures;
            }
            else
            {
                m_AverageRGBArray[count] = CalculateAverageColor(&subImage);
                count++;
            }
        }
        currentX = 0;
        currentY += m_subPictureHeight;
    }

}

int FindAvgColorsInMain::GetArraySize() const
{
    return m_numOfSubPics * m_noOfDividingPictures * m_noOfDividingPictures;
}

FindAvgColorsInMain::~FindAvgColorsInMain()
{

   // delete[] m_averageRGBArray;
}
