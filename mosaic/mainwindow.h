#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QGraphicsScene>
#include <QFileDialog>
#include <QString>
#include <QDebug>
#include <memory>
#include <QWidget>
#include <QVector>
QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:

    MainWindow(QWidget *Parent = nullptr);
    ~MainWindow();

public slots:

    void CheckingTheInput();
    void AskSubImageDirectory();
    void AskMainImageDirectory();

signals:

    void UpdateAll(QVector <QString> Vector);

private:

    std::shared_ptr<Ui::MainWindow> ui=std::make_shared<Ui::MainWindow>();

    bool AllInputsValid(int mainWidth, int mainHeight,int numberSubImage, QString ratio, QString mainImagePath, QString subImageDirectory) const;

    int m_mainWidth;
    int m_mainHeight;
    int m_numberSubImage;
    int m_subWidthRatio;
    int m_subHeightRatio;

    QVector<QString> VariablesMember;

    QGraphicsScene *Scene;

};
#endif // MAINWINDOW_H
