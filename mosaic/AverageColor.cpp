#include "AverageColor.h"
#include <QImage>
#include <QDebug>
#include <memory>
AverageColor::AverageColor(int subWidth, int subHeight, int noOfDividingPictures)
    :m_subPictureWidth(subWidth),m_subPictureHeight(subHeight),m_noOfDividingPictures(noOfDividingPictures){}

std::vector<AverageColor::RGB> AverageColor::GetAverageRGBArray() const
{
    return m_AverageRGBArray;
}

AverageColor::RGB AverageColor::CalculateAverageColor(QImage* picture) const
{
    RGB averageRGB ;
    auto sumRed = 0;
    auto sumGreen = 0;
    auto sumBlue = 0;

    for (auto index = 0; index < picture->width(); index++) {
        for (auto jndex = 0; jndex < picture->height(); jndex++) {
            QRgb pixel = picture->pixel(index, jndex);
            sumRed += qRed(pixel);
            sumGreen += qGreen(pixel);
            sumBlue += qBlue(pixel);
        }
    }

    averageRGB.R =  sumRed / (picture->width() * picture->height());
    averageRGB.G =  sumGreen / (picture->width() * picture->height());
    averageRGB.B =  sumBlue / (picture->width() * picture->height());

    return averageRGB;
}

std::vector<AverageColor::RGB> AverageColor::HigherPrecision(std::shared_ptr<QImage> subImage)
{
    uint8_t x = 0;
    uint8_t y = 0;
    auto width = subImage->width()/m_noOfDividingPictures;
    auto height = subImage->height()/m_noOfDividingPictures;

    std::vector<RGB> array ( m_noOfDividingPictures* m_noOfDividingPictures);
    uint8_t nr = 0;
    for(int index1 = 0; index1 < m_noOfDividingPictures ; index1++)
    {
        for(int index2 = 0; index2 < m_noOfDividingPictures ; index2++)
        {
            QImage image = CreateSubImages(subImage,x,y,height,width);
            x += width;

            array[nr] = CalculateAverageColor(&image);
            nr++;
        }
        x=0;
        y += height;
    }
    return array;
}

QImage AverageColor::CreateSubImages(std::shared_ptr<QImage> image, const int &xCoordinate,
                                     const int &yCoordinate, const int &height, const int &width) const
{
    auto first = xCoordinate * image->depth()/8;
    auto second = yCoordinate * image->bytesPerLine();
    size_t start=first + second;
    return QImage(image->bits()+start,width,height,image->bytesPerLine(), image->format());

}

AverageColor::~AverageColor()
{

}
