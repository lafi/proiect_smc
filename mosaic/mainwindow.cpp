#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *Parent)
    : QMainWindow(Parent)
{
    setWindowTitle("Photo MOSAIC");
    Scene = new QGraphicsScene(this);

    ui->setupUi(this);
    ui->PrecisionBox->setRange(1,20);

    connect(ui->getSubImgDir, &QPushButton::clicked,this, &MainWindow::AskMainImageDirectory);
    connect(ui->getImgDir, &QPushButton::clicked,this,&MainWindow::AskSubImageDirectory);
    connect(ui->step2, &QPushButton::clicked,this, &MainWindow::CheckingTheInput);
}

void MainWindow::AskSubImageDirectory()
{
    QString img_path = QFileDialog::getExistingDirectory(0,"Select directory", "/home", QFileDialog::ShowDirsOnly | QFileDialog::DontResolveSymlinks);
    ui->subImgDir->clear();
    ui->subImgDir->insert(img_path);
}

void MainWindow::AskMainImageDirectory()
{
    QString img_path = QFileDialog::getOpenFileName(0,"Select the main image","/home","Images (*.jpg *.jpeg *.png)");

    ui->ImgDir->clear();
    ui->ImgDir->insert(img_path);
}

bool MainWindow::AllInputsValid(int mainWidth, int mainHeight, int numberSubImage, QString ratio, QString mainImagePath, QString subImageDirectory)const
{
    int widthRatio = ratio.split(":").value(0).toInt();
    int heightRatio = ratio.split(":").value(1).toInt();

    bool ok = true;

    if(mainImagePath.isEmpty())
    {
        ui->ImgDir->clear();
        ok = false;
    }

    if(subImageDirectory.isEmpty())
    {
        ui->pmWidth->clear();
        ok = false;
    }

    if(ratio.size() < 3 )
    {
        ui->subRatio->insert("Please enter a valid ratio ( X : Y )");
    }

    if(mainWidth <= 0)
    {
        ui->pmWidth->clear();
        ui->pmWidth->setStyleSheet("Color: RED");
        ui->pmWidth->insert("Enter a valid width, please.. ( >0 )");
        ok = false;
    }

    if(mainHeight <= 0)
    {
        ui->pmHeight->clear();
        ui->pmHeight->setStyleSheet("Color: RED");
        ui->pmHeight->insert("Enter a valid height, please..( >0 )");
        ok = false;
    }

    if(numberSubImage <= 0){
        ui->numOfSub->clear();
        ui->numOfSub->setStyleSheet("Color: RED");
        ui->numOfSub->insert("Enter a valid height, please..( >0 )");
        ok = false;
    }

    if(widthRatio <= 0 || heightRatio <= 0){
        ui->subRatio->clear();
        ui->subRatio->setStyleSheet("Color: RED");
        ui->subRatio->insert("Enter a valid ratio, please..(X:Y)");
        ok = false;
    }

    return ok;

}

void MainWindow::CheckingTheInput()
{
    QString main_img=ui->ImgDir->text();
    QString sub_img=ui->subImgDir->text();
    if(sub_img.length() >= 1)
    {
        if(sub_img.at(sub_img.size()-1) != '/')
                sub_img.insert(sub_img.size(), '/');
    }
    int precision = ui->PrecisionBox ->value();
    int m_width = ui->pmWidth->text().toInt();
    int m_height = ui->pmHeight->text().toInt();
    int numberOf_subimg = ui->numOfSub->text().toInt();

    QString ratio = ui->subRatio->text();

    int w_ratio=ratio.split(":").value(0).toInt();
    int h_ratio=ratio.split(":").value(1).toInt();

    if(AllInputsValid(m_width,m_height,numberOf_subimg,ratio,main_img,sub_img))
    {
        ui->graphicsView->setScene(Scene);
        QPixmap image(main_img);
        Scene->addPixmap(image);

        ui->graphicsView->fitInView(Scene->itemsBoundingRect(), Qt::KeepAspectRatio);

        VariablesMember.append(QString::number(m_width));
        VariablesMember.append(QString::number(m_height));
        VariablesMember.append(QString::number(w_ratio));
        VariablesMember.append(QString::number(h_ratio));
        VariablesMember.append(QString::number(numberOf_subimg));
        VariablesMember.append(main_img);
        VariablesMember.append(sub_img);
        VariablesMember.append(QString::number(precision));

        qDebug() <<w_ratio << h_ratio;
        emit UpdateAll(VariablesMember);

        ui->pmHeight->setEnabled(false);
        ui->pmWidth->setEnabled(false);
        ui->subRatio->setEnabled(false);
        ui->numOfSub->setEnabled(false);
        ui->ImgDir->setEnabled(false);
        ui->subImgDir->setEnabled(false);
        ui->PrecisionBox->setEnabled(false);

        qDebug() << "All done!";

    }
}
MainWindow::~MainWindow()
{
    delete Scene;

}
