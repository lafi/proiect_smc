#include "AvgColorInDirectory.h"
#include <QFileInfoList>
AvgColorInDirectory::AvgColorInDirectory(QDir directory, int subPictureWidth, int subPictureHeight, QString format, int noOfPictures)
    :AverageColor(subPictureWidth,subPictureHeight,noOfPictures), m_Directory(directory), m_Format (format)  //uitat peste acest constructor
{
    directory.setSorting(QDir::Name);
    m_Directory.setSorting(QDir::Name);

    QStringList pictureOut;
    pictureOut << QString("*.") + format << QString("*.") + "jpg";

    m_Directory.setNameFilters(pictureOut);

    m_ArraySize = m_Directory.entryInfoList().size() * noOfPictures * noOfPictures;

    qDebug() << "Array size is: " << m_ArraySize;
    qDebug() << "The path is: " << m_Directory.path();
    qDebug() << "The input is: " << QDir::separator();

    m_AverageRGBArray = std::vector<RGB>(m_ArraySize);
}

void AvgColorInDirectory::CalculateALL()
{
    qDebug() << "Now we calculate the average colors for directory";
    ChangeResults();

    QFileInfoList informationList = m_Directory.entryInfoList();

    int position = 0;
    int size = informationList.size();

    for(int index = 0 ; index < size; index++){
        m_Picture->load(informationList.at(index).filePath());

        position = informationList.at(index).fileName().lastIndexOf('.');
        position = m_Directory.entryInfoList().at(index).fileName().left(position).toInt();

        if(m_noOfDividingPictures > 1 )
        {
            position = position*m_noOfDividingPictures*m_noOfDividingPictures;
            std::vector<RGB> array = HigherPrecision(m_Picture);

            for(int indexx = 0; indexx < (m_noOfDividingPictures*m_noOfDividingPictures); indexx++)
            {
                m_AverageRGBArray[position + indexx] = array[indexx];
            }
        }
        else
        {
            m_AverageRGBArray[position] = AverageColor::CalculateAverageColor(m_Picture.get());
        }
    }
}

void AvgColorInDirectory::ChangeResults()
{
    qDebug() << "Now we are changing the resolution of all images.";
    qDebug() << m_subPictureWidth;
    qDebug() << m_subPictureHeight;

    int nr = 0;
    QFileInfoList informationList = m_Directory.entryInfoList();
    int size = informationList.size();

    for(int index = 0 ; index < size; index++)
    {
        m_Picture->load(informationList.at(index).filePath());
        if(m_Picture->width() != m_subPictureWidth || m_Picture ->height() != m_subPictureHeight)
        {
            *m_Picture = m_Picture->scaled(m_subPictureWidth, m_subPictureHeight);
            m_Picture->save(informationList.at(index).filePath(),0,100);
            nr++;
        }
    }
    if(nr == 0)
    {
        qDebug() << "No images resized :(";
    }
    else
    {
        qDebug() << nr <<" Images resized ";
    }
}

int AvgColorInDirectory::GetArraySize() const
{
    return m_ArraySize;
}

AvgColorInDirectory::~AvgColorInDirectory()
{
   // delete[] m_averageRGBArray;
    //delete m_Picture;
}

