#include "ImageInformation.h"

ImageInformation::ImageInformation()
{

}

QString ImageInformation::MainImagePath() const
{
    return QFileDialog::getOpenFileName(0, "Select the Base picture", "/home", "Images (*.jpg *.jpeg *.png)");
}

void ImageInformation::UpdateAllImages(QVector<QString> vector)
{
    for (int i= 0; i < m_NumberOfSubImages; i++)

    {
        emit UpdateImageWidth(m_SubImageWidth);
        emit UpdateImageHeight(m_SubImageHeight);
        emit UpdateAllImages(vector);
    }
}

void ImageInformation::FindCharacteristics(int subImgWRatio, int subImgHRatio, int approxNumOfSubImages)
{
    double mainPicturePixels = m_MainImageWidth * m_MainImageHeight;
    double subPicturePixels = mainPicturePixels / approxNumOfSubImages;
    double pixelsForEachRatio = sqrt(subPicturePixels / (subImgWRatio * subImgHRatio));

    while (pixelsForEachRatio != ceil(pixelsForEachRatio))
    {
        approxNumOfSubImages++;
        subPicturePixels = mainPicturePixels / approxNumOfSubImages;
        pixelsForEachRatio = sqrt(subPicturePixels / (subImgWRatio * subImgHRatio));
    }

    m_SubImageWidth = pixelsForEachRatio * subImgWRatio;
    m_SubImageHeight = pixelsForEachRatio * subImgHRatio;
    m_NumberOfSubImages = approxNumOfSubImages;
}

QString ImageInformation::ImageDir() const
{
    return QFileDialog::getExistingDirectory(0, "Select the directory of pictures",
           "/home",QFileDialog::ShowDirsOnly|QFileDialog::DontResolveSymlinks);
}
void ImageInformation::SetAll(QVector<QString> Vec)
{
    qDebug() << "Width: " << Vec.value(0).toInt();
    qDebug() << "Height: " << Vec.value(1).toInt();
    qDebug() << "widthRatio: " << Vec.value(2).toInt();
    qDebug() << "heightRatio: " << Vec.value(3).toInt();
    qDebug() << "NumberOfImages: " << Vec.value(4).toInt();

    qDebug() << "Image path is: " << Vec.value(5);
    qDebug() << "Sub image directory is: " << Vec.value(6);

    m_MainImageWidth = Vec.value(0).toInt();
    m_MainImageHeight = Vec.value(1).toInt();

    int widthRatio = Vec.value(2).toInt();
    int heightRatio = Vec.value(3).toInt();
    int imageNumber = Vec.value(4).toInt();

    FindCharacteristics(widthRatio, heightRatio, imageNumber);

    qDebug() << m_SubImageWidth;
    qDebug() << m_SubImageHeight;
    qDebug() << m_NumberOfSubImages;

    emit UpdateImageWidth(m_SubImageWidth);
    emit UpdateImageHeight(m_SubImageHeight);

    Vec[4] = QString::number(m_NumberOfSubImages);
    UpdateAllImages(Vec);
}

