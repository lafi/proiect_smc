#ifndef AVERAGECOLOR_H
#define AVERAGECOLOR_H

#include <QImage>
#include <memory>
#include <vector>
class AverageColor
{

public:
    struct RGB
    {
        double R;
        double G;
        double B;
    };
    std::vector<RGB> GetAverageRGBArray() const;

protected:

    std::vector<RGB> m_AverageRGBArray;
    int m_subPictureWidth;
    int m_subPictureHeight;
    int m_noOfDividingPictures;

    RGB CalculateAverageColor(QImage* picture)const;
    AverageColor(int subWidht, int subHeight, int noOfDividingPictures);
    void virtual CalculateALL() = 0;
    int virtual GetArraySize() const = 0;

    std::shared_ptr<QImage> subImage;
    std::vector<RGB> HigherPrecision(std::shared_ptr<QImage> subImage);
    QImage CreateSubImages(std::shared_ptr<QImage> Image, const int &xCoordinate, const int &yCoordinate, const int &Height, const int &Width) const;

     virtual ~AverageColor();

};

#endif // AVERAGECOLOR_H
