#include "PhotoMosaic.h"

PhotoMosaic::PhotoMosaic()
{
    subImageWidth = 0;
    subImageHeight = 0;
}

void PhotoMosaic::SetSubImageWidth(int width)
{
    subImageWidth = width;
}

void PhotoMosaic::SetSubImageHeight(int height)
{
    subImageHeight = height;
}

void PhotoMosaic::RunConcurrent(QVector<QString> vector)
{
    QtConcurrent::run(this, &PhotoMosaic::Run, vector);
}

void PhotoMosaic::Run(QVector<QString> vector)
{
    int mainWidth = vector.value(0).toInt();
    int mainHeight = vector.value(1).toInt();

    int numOfSubImages = vector.value(4).toInt();

    QString mainImagePath = vector.value(5);
    QString subImageDir = vector.value(6);

    int precision = vector.value(7).toInt();

    int subWidth = subImageWidth;
    int subHeight = subImageHeight;

    mainImagePath = ResizeImg(mainImagePath, mainWidth, mainHeight);

    QDir directory(subImageDir);


    AvgColorInDirectory* averageDirectory = new AvgColorInDirectory(directory, subWidth, subHeight, "jpeg", precision);
    FindAvgColorsInMain* averageMain = new FindAvgColorsInMain(mainImagePath, subWidth, subHeight, numOfSubImages, precision);
    QFuture<void> avgDirThread = QtConcurrent::run(averageDirectory, &AvgColorInDirectory::CalculateALL);
    QFuture<void> avgMainThread = QtConcurrent::run(averageMain, &FindAvgColorsInMain::CalculateALL);

    QImage base;
    base.load(mainImagePath);
    std::shared_ptr<QImage> img2 ( new QImage(mainWidth, mainHeight, base.format()));

    qDebug() << "waiting ";
    avgDirThread.waitForFinished();
    avgMainThread.waitForFinished();
    std::vector<AverageColor::RGB> dirStruct = averageDirectory->GetAverageRGBArray();
    std::vector<AverageColor::RGB> mainStruct = averageMain->GetAverageRGBArray();
    QStringList* listRepeat = ColorDifference(dirStruct, averageDirectory->GetArraySize(), mainStruct, averageMain->GetArraySize(), (precision * precision));

    ConstructImage(listRepeat, img2, subImageDir);
    QString saveName = QString("%1(%2)%3")
        .arg("Photo Mosaic RGB")
        .arg(QString::number(precision))
        .arg(".jpg");
    qDebug() << saveName;
    img2->save(saveName);

    delete listRepeat;
    delete averageMain;
    delete averageDirectory;
}

QString PhotoMosaic::ResizeImg(QString img_path, int x, int y)const
{
    QImage img;
    img.load(img_path);

    if(img.width()==x && img.height()==y)
    {
        qDebug() << "no resizing required";
        return img_path;
    }

    QImage new_img = img.scaled(x,y);
    img_path = img_path.remove(".jpg");
    QString img_name = img_path+".jpg";
    new_img.save(img_name,0,100);
    return img_name;
}

void PhotoMosaic::ConstructImage(QStringList* sub_img, std::shared_ptr<QImage> destination, QString sub_ImgDir) const
{
    QPainter paint(destination.get());
    QImage img;
    int x = 0, y = 0;
    QPoint position;
    img.load(sub_ImgDir+sub_img->at(0)+".jpg");
    int imgY = destination->height()/img.height();
    int imgX = destination->width()/img.width();
    int number = 0;
    for(int i = 0; i < imgY; i++)
    {
        for(int j = 0; j < imgX; j++)
        {
            img.load(sub_ImgDir+sub_img->at(number)+".jpg");
            position = QPoint(x,y);
            paint.drawImage(position,img);
            x += img.width();
            number++;
        }
        y += img.height();
        x = 0;
    }
}

double PhotoMosaic::EuclideanDistanceRGB(AverageColor::RGB r1,AverageColor::RGB r2) const
{
    double r = pow((r1.R - r2.R), 2);
    double g = pow((r1.G - r2.G), 2);
    double b = pow((r1.B - r2.B), 2);
    double res = r + g + b;
    return sqrt(res);
}

QStringList* PhotoMosaic::ColorDifference(std::vector<AverageColor::RGB> dirArray, int dirArraySize,std::vector<AverageColor::RGB> mainArray, int mainArraySize, int precision) const
{
        qDebug() << "in colordifference";
        QStringList *list = new QStringList();

        int pos = 0;
        int percentageDone = 0;
        int onePercent = mainArraySize / 100;
        int count = onePercent;
        qDebug() << onePercent;
        for (auto index = 0; index < mainArraySize; index = index + precision)
        {
                if (index >= count)
                {
                    percentageDone++;
                    count += onePercent;
                    qDebug() << percentageDone;
                }

             auto min = 255 * precision * precision;
             for (auto jndex = 0; jndex < dirArraySize; jndex = jndex + precision)
             {
                double sum = 0;
                for (auto x = 0; x < precision; x++)
                    sum += EuclideanDistanceRGB(mainArray[index + x], dirArray[jndex + x]);
                if (sum < min)
                {
                     min = sum;
                     pos = jndex / precision;
                 }
                 sum = 0;
             }
            QString picName = QString::number(pos);
            list->append(picName);
         }
        return list;
}
