#ifndef IMAGEINFORMATION_H
#define IMAGEINFORMATION_H

#include <math.h>
#include <QDebug>
#include <QFileDialog>
#include <QString>
#include <QObject>
#include <QVector>
class ImageInformation : public QObject
{
    Q_OBJECT

public:

    ImageInformation();
    QString ImageDir() const;
    QString MainImagePath() const;
    void UpdateAllImages(QVector<QString> Vector);
public slots:

    void SetAll(QVector<QString> Vec);

signals:

    void UpdateImageWidth(int Width);
    void UpdateImageHeight(int Height);
    void UpdateNumberOfImages(int numOfImages);


private:

    void FindCharacteristics(int subImgWRatio, int subImgHRatio, int approxNumOfSubImages);

private:

    int m_MainImageWidth;
    int m_MainImageHeight;
    int m_SubImageWidth;
    int m_SubImageHeight;
    int m_NumberOfSubImages;
};

#endif
